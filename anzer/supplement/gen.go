// Thank robots for this file that was generated for you at 2019-07-20 15:58:38.418102108 +0300 MSK m=+0.014183988
package supplement

type TypeIn struct {
	Body    string `json:"body"`
	Brand   string `json:"brand"`
	Color   int    `json:"color"`
	Damages []struct {
		Damage string `json:"damage"`
		PartID int    `json:"partID"`
	} `json:"damages"`
	Description string `json:"description"`
	Generation  string `json:"generation"`
	Model       string `json:"model"`
	Photos      []struct {
		L string `json:"l"`
		M string `json:"m"`
		S string `json:"s"`
	} `json:"photos"`
	Price          int    `json:"price"`
	ProductionYear int    `json:"productionYear"`
	Pts            string `json:"pts"`
	Run            int    `json:"run"`
}

type TypeOut struct {
	Left *struct {
		AccidentDates []string `json:"accidentDates"`
		Body          string   `json:"body"`
		Brand         string   `json:"brand"`
		Color         int      `json:"color"`
		Damages       []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description string `json:"description"`
		Engine      struct {
			Fuel   int     `json:"fuel"`
			Turbo  bool    `json:"turbo"`
			Volume float64 `json:"volume"`
		} `json:"engine"`
		Generation string `json:"generation"`
		Model      string `json:"model"`
		ModelYear  int    `json:"modelYear"`
		Owners     int    `json:"owners"`
		Photos     []struct {
			L string `json:"l"`
			M string `json:"m"`
			S string `json:"s"`
		} `json:"photos"`
		Price          int    `json:"price"`
		ProductionYear int    `json:"productionYear"`
		Pts            string `json:"pts"`
		Run            int    `json:"run"`
	} `json:"left"`
	Right *struct {
		Err string `json:"err"`
	} `json:"right"`
}

func Handle(input TypeIn) TypeOut {
	return handle(input)
}
