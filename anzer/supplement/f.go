package supplement

import (
	"fmt"
	"math/rand"
	"time"
)

// 0 - бензин
// 1 - дизель
var engines = []struct {
	Fuel   int     `json:"fuel"`
	Turbo  bool    `json:"turbo"`
	Volume float64 `json:"volume"`
}{
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.4,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.4,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.6,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.6,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.8,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.8,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 2.0,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 2.0,
	},
	{
		Fuel:   1,
		Turbo:  false,
		Volume: 1.4,
	},
	{
		Fuel:   1,
		Turbo:  false,
		Volume: 2.0,
	},
}

const defaultYear = 2015

var modelYears = map[string]int{
	"Astra": 2012,
	"500":   2006,
	"Vesta": 2015,
	"DS3":   2009,
}

func handle(input TypeIn) TypeOut {
	out := TypeOut{}
	out.Left = &struct {
		AccidentDates []string `json:"accidentDates"`
		Body          string   `json:"body"`
		Brand         string   `json:"brand"`
		Color         int      `json:"color"`
		Damages       []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description string `json:"description"`
		Engine      struct {
			Fuel   int     `json:"fuel"`
			Turbo  bool    `json:"turbo"`
			Volume float64 `json:"volume"`
		} `json:"engine"`
		Generation string `json:"generation"`
		Model      string `json:"model"`
		ModelYear  int    `json:"modelYear"`
		Owners     int    `json:"owners"`
		Photos     []struct {
			L string `json:"l"`
			M string `json:"m"`
			S string `json:"s"`
		} `json:"photos"`
		Price          int    `json:"price"`
		ProductionYear int    `json:"productionYear"`
		Pts            string `json:"pts"`
		Run            int    `json:"run"`
	}{
		Body:           input.Body,
		Brand:          input.Brand,
		Color:          input.Color,
		Damages:        input.Damages,
		Description:    input.Description,
		Generation:     input.Generation,
		Model:          input.Model,
		Photos:         input.Photos,
		Price:          input.Price,
		ProductionYear: input.ProductionYear,
		Pts:            input.Pts,
		Run:            input.Run,
	}

	engine := rand.Intn(len(engines))
	out.Left.Engine = engines[engine]

	out.Left.ModelYear = defaultYear
	if year, ok := modelYears[out.Left.Model]; ok {
		out.Left.ModelYear = year
	}

	out.Left.Owners = int(1 + rand.Intn(3))
	accidentsNum := int(rand.Intn(3))

	for i := 0; i <= accidentsNum; i++ {
		date := time.Unix(int64(1000000000+rand.Intn(563642448)), 0)
		out.Left.AccidentDates = append(out.Left.AccidentDates, fmt.Sprint(date))
	}

	return out
}
