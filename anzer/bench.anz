package benchmark

type Raw = {
    brand        :: String
    model        :: String
    body         :: String
    pts          :: String
    creationYear :: Integer
    description  :: String
    price        :: Float
    color        :: String
    run          :: Integer
    damages      :: []{
        part        :: String
        description :: String
    }
    photos       :: []String
}

type Damage = {
  partID :: Integer
  damage :: String
}

type Transformed = {
    brand          :: String
    model          :: String
    generation     :: String
    body           :: String
    pts            :: String
    productionYear :: Integer
    description    :: String
    price          :: Integer
    color          :: Integer
    run            :: Integer
    damages        :: []Damage
    rawPhotos      :: []String
}

type Photo = {
    s :: String
    m :: String
    l :: String
}

type WithPhotos = {
    brand          :: String
    model          :: String
    generation     :: String
    body           :: String
    pts            :: String
    productionYear :: Integer
    description    :: String
    price          :: Integer
    color          :: Integer
    run            :: Integer
    damages        :: []Damage
    photos         :: []Photo
}

type Engine = {
    fuel   :: Integer
    volume :: Float
    turbo  :: Boolean
}

type Final = {
    brand          :: String
    model          :: String
    generation     :: String
    body           :: String
    pts            :: String
    modelYear      :: Integer
    productionYear :: Integer
    description    :: String
    price          :: Integer
    color          :: Integer
    run            :: Integer
    damages        :: []Damage
    accidentDates  :: []String
    owners         :: Integer
    photos         :: []Photo
    engine         :: Engine
}

type Advert = {
    id             :: Integer
    brand          :: String
    model          :: String
    generation     :: String
    body           :: String
    pts            :: String
    modelYear      :: Integer
    productionYear :: Integer
    description    :: String
    price          :: Integer
    color          :: Integer
    run            :: Integer
    damages        :: []Damage
    accidentDates  :: []String
    owners         :: Integer
    photos         :: []Photo
    engine         :: Engine
}

type Error = {
    err :: String
}

gitlab.com/anzer-cloud/benchmark/anzer/transform[go] :: Raw -> Either Transformed Error
gitlab.com/anzer-cloud/benchmark/anzer/load_photos[go] :: Transformed -> Either WithPhotos Error
gitlab.com/anzer-cloud/benchmark/anzer/supplement[go] :: WithPhotos -> Either Final Error
gitlab.com/anzer-cloud/benchmark/anzer/save[go] :: Final -> Either Advert Error

create_advert = transform >>= load_photos >>= supplement >>= save

invoke(create_advert,)
