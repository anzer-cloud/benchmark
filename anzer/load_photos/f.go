package load_photos

import (
	"fmt"
	"github.com/twinj/uuid"
)

const baseUrl = "http://static.storage.org"

func handle(input TypeIn) TypeOut {
	out := TypeOut{}

	out.Left = &struct {
		Body    string `json:"body"`
		Brand   string `json:"brand"`
		Color   int    `json:"color"`
		Damages []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description string `json:"description"`
		Generation  string `json:"generation"`
		Model       string `json:"model"`
		Photos      []struct {
			L string `json:"l"`
			M string `json:"m"`
			S string `json:"s"`
		} `json:"photos"`
		Price          int    `json:"price"`
		ProductionYear int    `json:"productionYear"`
		Pts            string `json:"pts"`
		Run            int    `json:"run"`
	}{
		Body:           input.Body,
		Brand:          input.Brand,
		Color:          input.Color,
		Damages:        input.Damages,
		Description:    input.Description,
		Generation:     input.Generation,
		Model:          input.Model,
		Price:          input.Price,
		ProductionYear: input.ProductionYear,
		Pts:            input.Pts,
		Run:            input.Run,
	}

	// Mock the photos loader
	for range input.RawPhotos {
		id := uuid.NewV4().String()
		photo := struct {
			L string `json:"l"`
			M string `json:"m"`
			S string `json:"s"`
		}{
			L: fmt.Sprintf("%s/l/%s.jpeg", baseUrl, id),
			M: fmt.Sprintf("%s/m/%s.jpeg", baseUrl, id),
			S: fmt.Sprintf("%s/s/%s.jpeg", baseUrl, id),
		}
		out.Left.Photos = append(out.Left.Photos, photo)
	}

	return out
}
