Anzer variant

Example body of the query:

```json
{
  "brand": "Opel",
  "model": "Astra J restiling",
  "body": "Хэтчбэк",
  "pts": "65487SDEXAMPLE",
  "creationYear": 2014,
  "description": "Абсолютно новая тачка!!! Не бита ни крашена!!!",
  "price": 400000.5,
  "color": "красный",
  "run": 100500,
  "damages": [
    {
      "part": "крыша",
      "description": "побита слегка"
    },
    {
      "part": "дверь",
      "description": "дверь от семёрки"
    }
  ],
  "photos": [
    "http://super-foto-bank.org/astra1.jpeg",
    "http://super-foto-bank.org/astra2.jpeg",
    "http://super-foto-bank.org/astra3.jpeg"
  ]
} 
```