package save

import (
	"crypto/rand"
	"math/big"
)

func handle(input TypeIn) TypeOut {
	out := TypeOut{}

	r, err := rand.Int(rand.Reader, big.NewInt(15))
	if err != nil {
		out.Right = &struct {
			Err string `json:"err"`
		}{
			Err: err.Error(),
		}
		return out
	}

	if r.Int64() == 5 {
		out.Right = &struct {
			Err string `json:"err"`
		}{
			Err: "Random number is 5, so this is the error",
		}
		return out
	}

	r, _ = rand.Int(rand.Reader, big.NewInt(1005000))

	out.Left = &struct {
		AccidentDates []string `json:"accidentDates"`
		Body          string   `json:"body"`
		Brand         string   `json:"brand"`
		Color         int      `json:"color"`
		Damages       []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description string `json:"description"`
		Engine      struct {
			Fuel   int     `json:"fuel"`
			Turbo  bool    `json:"turbo"`
			Volume float64 `json:"volume"`
		} `json:"engine"`
		Generation string `json:"generation"`
		Id         int    `json:"id"`
		Model      string `json:"model"`
		ModelYear  int    `json:"modelYear"`
		Owners     int    `json:"owners"`
		Photos     []struct {
			L string `json:"l"`
			M string `json:"m"`
			S string `json:"s"`
		} `json:"photos"`
		Price          int    `json:"price"`
		ProductionYear int    `json:"productionYear"`
		Pts            string `json:"pts"`
		Run            int    `json:"run"`
	}{
		AccidentDates:  input.AccidentDates,
		Body:           input.Body,
		Brand:          input.Brand,
		Color:          input.Color,
		Damages:        input.Damages,
		Description:    input.Description,
		Engine:         input.Engine,
		Generation:     input.Generation,
		Model:          input.Model,
		ModelYear:      input.ModelYear,
		Owners:         input.Owners,
		Photos:         input.Photos,
		Price:          input.Price,
		ProductionYear: input.ProductionYear,
		Pts:            input.Pts,
		Run:            input.Run,
		Id:             int(r.Int64()),
	}
	return out
}
