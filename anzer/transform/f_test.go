package transform

import (
	"testing"
)

func TestExtractModel(t *testing.T) {
	model, gen, err := extractModel("Astra J Restiling")
	if err != nil {
		t.Error(err)
	}
	if model != "Astra" {
		t.Error("Model is", model)
	}

	if gen != "J Restiling" {
		t.Error("Generation is", gen)
	}
}
