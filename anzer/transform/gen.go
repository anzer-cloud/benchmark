// Thank robots for this file that was generated for you at 2019-07-20 15:56:42.611449447 +0300 MSK m=+0.040997868
package transform

type TypeIn struct {
	Body         string `json:"body"`
	Brand        string `json:"brand"`
	Color        string `json:"color"`
	CreationYear int    `json:"creationYear"`
	Damages      []struct {
		Description string `json:"description"`
		Part        string `json:"part"`
	} `json:"damages"`
	Description string   `json:"description"`
	Model       string   `json:"model"`
	Photos      []string `json:"photos"`
	Price       float64  `json:"price"`
	Pts         string   `json:"pts"`
	Run         int      `json:"run"`
}

type TypeOut struct {
	Left *struct {
		Body    string `json:"body"`
		Brand   string `json:"brand"`
		Color   int    `json:"color"`
		Damages []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description    string   `json:"description"`
		Generation     string   `json:"generation"`
		Model          string   `json:"model"`
		Price          int      `json:"price"`
		ProductionYear int      `json:"productionYear"`
		Pts            string   `json:"pts"`
		RawPhotos      []string `json:"rawPhotos"`
		Run            int      `json:"run"`
	} `json:"left"`
	Right *struct {
		Err string `json:"err"`
	} `json:"right"`
}

func Handle(input TypeIn) TypeOut {
	return handle(input)
}
