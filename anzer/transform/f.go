package transform

import (
	"errors"
	"regexp"
	"strings"
)

var (
	generationR1 = regexp.MustCompile(`(?i)([\w\s]+) (\w+) (\d+) (restiling|рестайлинг)`)
	generationR2 = regexp.MustCompile(`(?i)([\w\s]+) (\w+) (restiling|рестайлинг)`)

	damagesMap = map[string]int{
		"крыша":  1,
		"крыло":  2,
		"бампер": 3,
		"капот":  4,
		"дверь":  5,
	}
	colorsMap = map[string]int{
		"красный": 1,
		"зеленый": 2,
		"белый":   3,
		"черный":  4,
		"серый":   5,
	}
)

func handle(input TypeIn) TypeOut {
	out := TypeOut{}

	model, generation, err := extractModel(input.Model)
	if err != nil {
		out.Right = &struct {
			Err string `json:"err"`
		}{
			Err: err.Error(),
		}
		return out
	}

	out.Left = &struct {
		Body    string `json:"body"`
		Brand   string `json:"brand"`
		Color   int    `json:"color"`
		Damages []struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		} `json:"damages"`
		Description    string   `json:"description"`
		Generation     string   `json:"generation"`
		Model          string   `json:"model"`
		Price          int      `json:"price"`
		ProductionYear int      `json:"productionYear"`
		Pts            string   `json:"pts"`
		RawPhotos      []string `json:"rawPhotos"`
		Run            int      `json:"run"`
	}{
		Body:           input.Body,
		Brand:          input.Brand,
		Description:    input.Description,
		Generation:     generation,
		Model:          model,
		ProductionYear: input.CreationYear,
		Pts:            input.Pts,
		RawPhotos:      input.Photos,
		Run:            input.Run,
		Color:          -1,
		Price:          int(input.Price),
	}

	for _, d := range input.Damages {
		newD := struct {
			Damage string `json:"damage"`
			PartID int    `json:"partID"`
		}{
			Damage: d.Description,
			PartID: -1,
		}
		if id, ok := damagesMap[d.Part]; ok {
			newD.PartID = id
		}
		out.Left.Damages = append(out.Left.Damages, newD)
	}

	color := strings.Replace(input.Color, "ё", "е", -1)
	if colorid, ok := colorsMap[color]; ok {
		out.Left.Color = colorid
	}

	return out
}

func extractModel(model string) (string, string, error) {
	matches := generationR1.FindAllStringSubmatch(model, -1)
	if len(matches) > 0 && len(matches[0]) >= 2 {
		return matches[0][1], strings.Join(matches[0][2:], " "), nil
	}

	matches = generationR2.FindAllStringSubmatch(model, -1)
	if len(matches) > 0 && len(matches[0]) >= 2 {
		return matches[0][1], strings.Join(matches[0][2:], " "), nil
	}

	return "", "", errors.New("car model is invalid")
}
