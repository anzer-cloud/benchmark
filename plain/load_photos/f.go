package main

import (
	"fmt"
	"github.com/twinj/uuid"
)

const baseUrl = "http://static.storage.org"

func Main(obj map[string]interface{}) map[string]interface{} {
	if photos, ok := obj["rawPhotos"].([]interface{}); ok {
		newPhotos := []map[string]interface{}{}
		for range photos {
			id := uuid.NewV4().String()
			newPhotos = append(newPhotos, map[string]interface{}{
				"l": fmt.Sprintf("%s/l/%s.jpeg", baseUrl, id),
				"m": fmt.Sprintf("%s/m/%s.jpeg", baseUrl, id),
				"s": fmt.Sprintf("%s/s/%s.jpeg", baseUrl, id),
			})
		}
		obj["photos"] = newPhotos
		delete(obj, "rawPhotos")
	}
	return obj
}
