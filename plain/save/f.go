package main

import (
	"crypto/rand"
	"math/big"
)

func Main(obj map[string]interface{}) map[string]interface{} {
	r, err := rand.Int(rand.Reader, big.NewInt(15))
	if err != nil {
		return map[string]interface{}{
			"err": err,
		}
	}

	if r.Int64() == 5 {
		return map[string]interface{}{
			"err": "Random number is 5, so this is the error",
		}
	}

	r, _ = rand.Int(rand.Reader, big.NewInt(1005000))
	obj["id"] = int(r.Int64())

	return obj
}
