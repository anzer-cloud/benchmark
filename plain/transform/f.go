package main

import (
	"errors"
	"regexp"
	"strings"
)

var (
	generationR1 = regexp.MustCompile(`(?i)([\w\s]+) (\w+) (\d+) (restiling|рестайлинг)`)
	generationR2 = regexp.MustCompile(`(?i)([\w\s]+) (\w+) (restiling|рестайлинг)`)

	damagesMap = map[string]int{
		"крыша":  1,
		"крыло":  2,
		"бампер": 3,
		"капот":  4,
		"дверь":  5,
	}
	colorsMap = map[string]int{
		"красный": 1,
		"зеленый": 2,
		"белый":   3,
		"черный":  4,
		"серый":   5,
	}
)

func Main(obj map[string]interface{}) map[string]interface{} {
	var model, generation string
	if sourceModel, ok := obj["model"].(string); ok {
		var err error
		model, generation, err = extractModel(sourceModel)
		if err != nil {
			return map[string]interface{}{
				"err": err,
			}
		}
	}

	ret := map[string]interface{}{
		"brand":          obj["brand"],
		"model":          model,
		"generation":     generation,
		"body":           obj["body"],
		"pts":            obj["pts"],
		"productionYear": obj["productionYear"],
		"description":    obj["description"],
		"price":          obj["price"],
		"color":          obj["color"],
		"run":            obj["run"],
		"rawPhotos":      obj["photos"],
		"damages":        []map[string]interface{}{},
	}

	if ds, ok := obj["damages"].([]interface{}); ok {
		newDs := []map[string]interface{}{}
		for _, d := range ds {
			if dd, ok := d.(map[string]interface{}); ok {
				pid := -1
				if part, ok := dd["part"].(string); ok {
					if id, ok := damagesMap[part]; ok {
						pid = id
					}
				}

				newDs = append(newDs, map[string]interface{}{
					"part":        pid,
					"description": dd["description"],
				})
			}

		}
		ret["damages"] = newDs
	}

	if color, ok := obj["color"].(string); ok {
		color = strings.Replace(color, "ё", "е", -1)
		if colorid, ok := colorsMap[color]; ok {
			ret["color"] = colorid
		}
	}

	return ret
}

func extractModel(model string) (string, string, error) {
	matches := generationR1.FindAllStringSubmatch(model, -1)
	if len(matches) > 0 && len(matches[0]) >= 2 {
		return matches[0][1], strings.Join(matches[0][2:], " "), nil
	}

	matches = generationR2.FindAllStringSubmatch(model, -1)
	if len(matches) > 0 && len(matches[0]) >= 2 {
		return matches[0][1], strings.Join(matches[0][2:], " "), nil
	}

	return "", "", errors.New("car model is invalid")
}
