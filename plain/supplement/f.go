package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 0 - бензин
// 1 - дизель
var engines = []struct {
	Fuel   int     `json:"fuel"`
	Turbo  bool    `json:"turbo"`
	Volume float64 `json:"volume"`
}{
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.4,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.4,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.6,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.6,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 1.8,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 1.8,
	},
	{
		Fuel:   0,
		Turbo:  false,
		Volume: 2.0,
	},
	{
		Fuel:   0,
		Turbo:  true,
		Volume: 2.0,
	},
	{
		Fuel:   1,
		Turbo:  false,
		Volume: 1.4,
	},
	{
		Fuel:   1,
		Turbo:  false,
		Volume: 2.0,
	},
}

const defaultYear = 2015

var modelYears = map[string]int{
	"Astra": 2012,
	"500":   2006,
	"Vesta": 2015,
	"DS3":   2009,
}

func Main(obj map[string]interface{}) map[string]interface{} {
	engine := rand.Intn(len(engines))
	obj["engine"] = engines[engine]

	obj["year"] = defaultYear
	if model, ok := obj["model"].(string); ok {
		if year, ok := modelYears[model]; ok {
			obj["year"] = year
		}
	}

	obj["owners"] = int(1 + rand.Intn(3))

	accidentsNum := int(rand.Intn(3))
	accidents := []string{}
	for i := 0; i <= accidentsNum; i++ {
		date := time.Unix(int64(1000000000+rand.Intn(563642448)), 0)
		accidents = append(accidents, fmt.Sprint(date))
	}
	obj["accidents"] = accidents

	return obj
}
