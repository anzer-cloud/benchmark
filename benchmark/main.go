package main

import (
	"encoding/csv"
	"fmt"
	"github.com/valyala/fasthttp"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// run 100 query1.json report.csv https://service.eu.apiconnect.ibmcloud.com/gws/apigateway/api/abd5de6a5895f12422051170895482b2424b9969eb74da4e0b3598bcae04a820/adv
func main() {
	if len(os.Args) < 5 {
		panic("no args")
	}
	url := os.Args[4]

	file := os.Args[2]
	d, err := ioutil.ReadFile(file)
	die(err)

	count, err := strconv.Atoi(os.Args[1])
	die(err)

	csvfile, err := os.Create(os.Args[3])
	die(err)
	defer csvfile.Close()

	csvwriter := csv.NewWriter(csvfile)
	defer csvwriter.Flush()

	c := &fasthttp.Client{}
	req := &fasthttp.Request{}
	req.SetBody(d)
	req.SetRequestURI(url)
	req.Header.Set("Content-Type", "application/json")
	req.Header.SetMethod(fasthttp.MethodPost)

	for i := 0; i < count; i++ {
		resp := &fasthttp.Response{}

		start := time.Now()
		err := c.Do(req, resp)
		duration := time.Since(start)

		die(err)
		if resp.StatusCode() >= 400 {
			fmt.Printf("Status is %d\n", resp.StatusCode)
			continue
		}

		fmt.Print(".")
		dur := int(duration.Nanoseconds()) / int(time.Millisecond)
		err = csvwriter.Write([]string{strconv.Itoa(dur)})
		die(err)
		tts := time.Millisecond * time.Duration(rand.Intn(9000))
		time.Sleep(tts)
	}
	fmt.Println()
}

func die(err error) {
	if err != nil {
		panic(err)
	}
}
