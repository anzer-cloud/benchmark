module gitlab.com/anzer-cloud/benchmark/benchmark

go 1.12

require (
	github.com/klauspost/compress v1.7.4 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/valyala/fasthttp v1.4.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190712062909-fae7ac547cb7 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190719005602-e377ae9d6386 // indirect
)
